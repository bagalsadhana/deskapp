package gnukhata.views;

import gnukhata.globals;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class ViewCashFlowReport extends Composite {
	String strOrgName;
	String strToYear;
	
	Table CashFlowTable;
	TableColumn AccName;
	TableColumn Amount;
	TableColumn AccName1;
	TableColumn Amount1;
	TableItem headerRow;
	
	Label lblLogo;
	Label lblLink ;
	Label lblLine;
	Label lblaccname;
	Label lblaccname1;
	Label lblamount;
	Label lblamount1;
	Label lblPeriod;
	Label lblstmt;
	Label lblReceipt;
	Label lblPayment;
	Label lblorgname;
	Button btnViewcashflowForAccount;
	//Button btnPrint;

	
	TableEditor AccnameEditor;
	TableEditor AmountEditor;
	TableEditor AccnameEditor1;
	TableEditor AmountEditor1;
	//TableEditor VoucherNumberEditor;
	//TableEditor DrEditotr;
	
static Display display;
	
	Button btnViewLedgerForAccount;
	Button btnPrint;
	
	static String frmDate;
	String strFromDate;
	String strToDate;
	String toDate;
	String endDateParam = "";
	Vector<Object> printcashflow = new Vector<Object>();
	public ViewCashFlowReport(Composite parent, int style,Object[] result, String fromDate, String toDate, String financialFrom) {
		// TODO Auto-generated constructor stub
		
		super(parent,style);
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		FormData layout = new FormData();
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(67);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		String strdate;
		strdate=toDate.substring(8)+"-"+toDate.substring(5,7)+"-"+toDate.substring(0, 4);
		endDateParam = toDate;
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3]);
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("---------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(14);
		lblLine.setLayoutData(layout);
		
		//strToDate=fromDate.substring(8)+"-"+fromDate.substring(5,7)+"-"+fromDate.substring(0,4);
		
		Label lblstmt = new Label(this, SWT.NONE);
		lblstmt.setText("RECEIPT AND PAYMENT ACCOUNT FOR THE PERIOD ENDED  " + strdate);
		lblstmt.setFont(new Font(display, "Times New Roman", 13, SWT.ITALIC| SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblorgname,2);
		layout.left = new FormAttachment(16);
		//layout.right = new FormAttachment(85);
		//layout.bottom = new FormAttachment(22);
		lblstmt.setLayoutData(layout);
		
		Label lblReceipt = new Label(this, SWT.NONE);
		lblReceipt.setText(" RECEIPTS ");
		lblReceipt.setFont(new Font(display, "Times New Roman", 13, SWT.ITALIC|SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblstmt,12);
		layout.left = new FormAttachment(16);
		//layout.right = new FormAttachment(19);
		//layout.bottom = new FormAttachment(22);
		lblReceipt.setLayoutData(layout);
		
		Label lblPayment = new Label(this, SWT.NONE);
		lblPayment.setText(" PAYMENTS ");
		lblPayment.setFont(new Font(display, "Times New Roman", 13, SWT.ITALIC |SWT.BOLD));
		layout = new FormData();
		layout.top = new FormAttachment(lblstmt,12);
		layout.left = new FormAttachment(60);
//		layout.right = new FormAttachment(20);
//		//layout.bottom = new FormAttachment(22);
		lblPayment.setLayoutData(layout);
		
		CashFlowTable= new Table(this, SWT.MULTI|SWT.BORDER|SWT.LINE_SOLID|SWT.FULL_SELECTION);
		CashFlowTable.setLinesVisible (true);
		CashFlowTable.setHeaderVisible (false);
		layout = new FormData();
		layout.top = new FormAttachment(lblReceipt,5);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(92);
		layout.bottom = new FormAttachment(85);
		CashFlowTable.setLayoutData(layout);
		
				
		btnViewcashflowForAccount =new Button(this,SWT.PUSH);
		btnViewcashflowForAccount.setText("&Back To CashFlow");
		btnViewcashflowForAccount.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(CashFlowTable,10);
		layout.left=new FormAttachment(25);
		btnViewcashflowForAccount.setLayoutData(layout);
		btnViewcashflowForAccount.setFocus();

		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",14,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(CashFlowTable,10);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);

		this.makeaccessible(CashFlowTable);
		this.getAccessible();
		
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		this.setReport(result);
		
		this.setEvents();
		//this.pack();
	}
	
	private void setReport(Object[]result)
	{
		String[] columns;
		String[] orgname;
		orgname=new String[]{ globals.session[1].toString()};
		columns=new String[]{"Account Name","Amount","Account Name","Amount"};
		
		lblaccname = new Label(CashFlowTable,SWT.BORDER|SWT.CENTER);
		lblaccname.setFont(new Font(display, "Times New Roman",12,SWT.BOLD));
		lblaccname.setText("Name of Accounts");
		
		lblamount = new Label(CashFlowTable, SWT.BORDER|SWT.CENTER);
		lblamount.setFont(new Font(display, "Times New Roman",12,SWT.BOLD));
		lblamount.setText("Amounts");
		
		lblaccname1 = new Label(CashFlowTable,SWT.BORDER|SWT.CENTER);
		lblaccname1.setFont(new Font(display, "Times New Roman",12,SWT.BOLD));
		lblaccname1.setText("Name of Accounts");
		
		lblamount1 = new Label(CashFlowTable, SWT.BORDER|SWT.CENTER);
		lblamount1.setFont(new Font(display, "Times New Roman",12,SWT.BOLD));
		lblamount1.setText("Amounts");
		
		headerRow = new TableItem(CashFlowTable, SWT.NONE);
		
		TableItem[] items = CashFlowTable.getItems();
		
		AccName = new TableColumn(CashFlowTable, SWT.BORDER);
		Amount = new TableColumn(CashFlowTable, SWT.RIGHT);
		AccName1 = new TableColumn(CashFlowTable, SWT.BORDER);
		Amount1 = new TableColumn(CashFlowTable, SWT.RIGHT);
		
		AccnameEditor = new TableEditor(CashFlowTable);
		AccnameEditor.grabHorizontal = true;
		AccnameEditor.setEditor(lblaccname,items[0],0);
		
		AmountEditor = new TableEditor(CashFlowTable);
		AmountEditor.grabHorizontal = true;
		AmountEditor.setEditor(lblamount,items[0],1);
		
		AccnameEditor1 = new TableEditor(CashFlowTable);
		AccnameEditor1.grabHorizontal = true;
		AccnameEditor1.setEditor(lblaccname1,items[0],2);
		
		AmountEditor1 = new TableEditor(CashFlowTable);
		AmountEditor1.grabHorizontal = true;
		AmountEditor1.setEditor(lblamount1,items[0],3);
		
		final int tblwidth = CashFlowTable.getClientArea().width;
		CashFlowTable.addListener(SWT.MeasureItem, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				// TODO Auto-generated method stub
				
				AccName.setWidth(30 * tblwidth / 100);
				Amount.setWidth(20 * tblwidth / 100);
				AccName1.setWidth(30 * tblwidth / 100);
				Amount1.setWidth(20 * tblwidth / 100);
				event.height = 12;
			}
		});
			
		AccName.pack();
		Amount.pack();
		AccName1.pack();
		Amount1.pack();


		Object[] rlist = (Object[]) result[0];
		Object plist[] = (Object[]) result[1];
		Integer difflen = 0;
		if(rlist.length > plist.length)
		{
			difflen =  rlist.length;
		}
		else
		{
			difflen = plist.length;
		}
		btnPrint.setData("printorgname",orgname);
		btnPrint.setData("printcolumns",columns);
		for(int rowcounter =0; rowcounter < difflen; rowcounter ++)
		{
			
			TableItem cashRow = new TableItem(CashFlowTable, SWT.NONE);
			
			
			Object[] receipts = (Object[]) rlist[rowcounter]; 
			Object[] payments = (Object[]) plist[rowcounter]; 

			if(rowcounter==difflen-1)
			{
				cashRow.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
			}
			//Code to display records of receipts side
			if(receipts[0].toString().equals("ob"))
			{
				cashRow.setText(0, "      	   " + receipts[1].toString());
				cashRow.setText(1, receipts[2].toString());
				Object[] printableRow = new Object[]{receipts[1],receipts[2]};
				printcashflow.add(printableRow);
			}
			else
			{
				cashRow.setText(0, receipts[0].toString());
				cashRow.setText(1, receipts[1].toString());
				Object[] printableRow = new Object[]{receipts[0],receipts[1]};
				printcashflow.add(printableRow);
			}			
		
			//Code to display records of payments side
			if(payments[0].toString().equals("cb"))
			{
				cashRow.setText(2, "      	   " + payments[1].toString());
				cashRow.setText(3, payments[2].toString());
				Object[] printableRow = new Object[]{"","",payments[1],payments[2]};
				printcashflow.add(printableRow);
			}
			else
			{
				
				cashRow.setText(2, payments[0].toString());
				cashRow.setText(3, payments[1].toString());
				Object[] printableRow = new Object[]{"","",payments[0],payments[1]};
				printcashflow.add(printableRow);
			}			
		}
		//CashFlowTable.pack();
		//CashFlowTable.setSize(93, 83);
	}
	
	private void setEvents()
	{
		btnViewcashflowForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewcashflowForAccount.getParent().getParent();
				btnViewcashflowForAccount.getParent().dispose();
					
					viewCashflow vl=new viewCashflow(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
			
			}
		
		});
//CashFlowTable.setFocus();
		
		btnViewcashflowForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
				    btnPrint.setFocus();
				}
					
			}
		});
		
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewcashflowForAccount.setFocus();
				}
			}
		});
		btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				String[] orgname=(String[])btnPrint.getData("printorgname");
				String[] columns = (String[]) btnPrint.getData("printcolumns");
				Object[][] orgdata =new Object[printcashflow.size()][orgname.length ];
				Object[][] finalData =new Object[printcashflow.size()][columns.length];
				for(int counter = 0; counter < printcashflow.size(); counter ++ )
				{
					Object[] printRow = (Object[]) printcashflow.get(counter);												
					finalData[counter] = printRow;
				}
					
				//printLedgerData.copyInto(finalData);
				
					TableModel model = new DefaultTableModel(finalData,columns);
	/*				try {
						final File cashflow = new File("cashflow.ods" );
						final File cashflowtemplate = new File("Report_Templates/CashFlow.ots"); 
						//SpreadSheet.createEmpty(model).saveAs(cashflow);
						final Sheet cashSheet = SpreadSheet.createFromFile(cashflowtemplate).getSheet(0);
						cashSheet.ensureRowCount(100000);
						ledgerSheet.getColumn(1).setWidth(new Integer(40));
						ledgerSheet.getColumn(2).setWidth(new Integer(40));
						cashSheet.getCellAt(0,0).setValue(globals.session[1].toString());
						cashSheet.getCellAt(0,0).setValue("Receipt And Payment Account For The Period Ended "+globals.session[3].toString());
						for(int rowcounter=0; rowcounter<printcashflow.size();)
						{
							Object[] printrow = (Object[]) printcashflow.get(rowcounter);
							cashSheet.getCellAt(0,rowcounter+4).setValue(printrow[0].toString());
							cashSheet.getCellAt(1,rowcounter+4).setValue(printrow[1].toString());
							cashSheet.getCellAt(2,rowcounter+4).setValue(printrow[2].toString());
							cashSheet.getCellAt(3,rowcounter+4).setValue(printrow[3].toString());
						}
						OOUtils.open(cashSheet.getSpreadSheet().saveAs(cashflow));

						//OOUtils.open(cashflow);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
			}
		});
		
	}
		
	public void makeaccessible(Control c) {
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}
	
	protected void checkSubclass() {
		// this is blank method so will disable the check that prevents
		// subclassing of shells.
	}

	
	
	
		
	
}